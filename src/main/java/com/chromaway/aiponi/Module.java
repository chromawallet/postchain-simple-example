package com.chromaway.aiponi;

import net.postchain.core.EContext;
import net.postchain.core.Transactor;
import net.postchain.core.UserMistake;
import net.postchain.gtx.*;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

import static net.postchain.gtx.ValuesKt.gtx;

public class Module implements GTXModule {

    private final Set<String> queries = new HashSet<String>();
    private final Set<String> operations = new HashSet<String>();

    Module() {
        operations.add("add_payment");
    }

    @NotNull
    @Override
    public Set<String> getQueries() { return queries; }

    @NotNull
    @Override
    public Set<String> getOperations() { return operations;  }

    @NotNull
    @Override
    public Transactor makeTransactor(ExtOpData extOpData) {
        String s = extOpData.getOpName();
        if (s.equals("add_payment")){
            return new AddPaymentOperation(extOpData);
        }else {
            throw new UserMistake("Operation not found", null);
        }
    }

    @NotNull
    @Override
    public GTXValue query(EContext eContext, String name, GTXValue gtxValue) {
        return gtxValue;
    }

    @Override
    public void initializeDB(EContext eContext) {
        GTXSchemaManager.INSTANCE.autoUpdateSQLSchema(eContext, 0,
                getClass(), "schema.sql", getClass().getName()
        );
    }
}