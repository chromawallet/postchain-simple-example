# simple example

## Aiponi

This simple example shows just how little code is needed for integration of application and database logic with postchain: 4 files and a total of 134 lines of code, including the SQL (which all could be made shorter).

The code is in Java, but could also have been written in Kotlin or another JVM language.

Warning: This code is from a summer intern's project, and at the time of this writing, has not been vetted by our devs.

## What it does

Aiponi receives transaction messages of the format:

    add_payment(identification, payment, amount, record_date)

This is inserted in the PostgreSQL database ```phone_payments```, as long as the message does not trigger an exception. If it does trigger an exception, the Postchain node votes no to the transaction, and should not be entered into the blockchain.

## Everything in 4 files, with a total of 134 lines of code

The files that completely define the customization of postchain are here:

* [Module factory, entry point](./src/main/java/com/chromaway/aiponi/ModuleFactory.java)
* [Module](./src/main/java/com/chromaway/aiponi/Module.java)
* [Application logic](./src/main/java/com/chromaway/aiponi/AddPaymentOperation.java)
* [SQL](.src/main/resources/com/chromaway/aiponi/schema.sql)


## Under the hood

The postchain libraries take care of everything else: Communication between the blockchain nodes, including byzantine fault tolerant voting, decoding of client messages and so on.