CREATE TABLE IF NOT EXISTS phone_payments (
  identification TEXT NOT NULL,
  record_date BIGINT NOT NULL,
  payment BIGINT NOT NULL,
  amount BIGINT NOT NULL
);

CREATE OR REPLACE FUNCTION add_payment(p_identification TEXT, p_timestamp BIGINT, p_payment BIGINT, p_amount BIGINT)
RETURNS VOID AS $$
BEGIN
  INSERT INTO phone_payments(identification, record_date, payment, amount) VALUES (p_identification, p_timestamp, p_payment, p_amount);
END;
$$ LANGUAGE plpgsql;